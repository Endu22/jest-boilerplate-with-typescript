// Find the amount of Friday the 13th in a year

export function howUnlucky(year) {
  if (isNaN(year)) {
    throw new Error("Input is not a valid year.")
  }

  let counter = 0
  for (let i = 1; i <= 12; i++) {
    let date = ""

    i > 9
      ? (date = new Date(`${year}-${i}-13`))
      : (date = new Date(`${year}-0${i}-13`))

    if (date.getDay() === 5) counter++
  }
  return counter
}
