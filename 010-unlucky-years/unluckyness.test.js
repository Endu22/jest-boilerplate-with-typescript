// howUnlucky(2020) ➞ 2
// howUnlucky(2026) ➞ 3
// howUnlucky(2016) ➞ 1

import { howUnlucky } from "./unluckyness"


it("Should return 2 when given '2020'", () => {
    // Arrange
    var input = 2020
    var expected = 2
    
    // Act
    var actual = howUnlucky(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 3 when given '2026'", () => {
    // Arrange
    var input = 2026
    var expected = 3
    
    // Act
    var actual = howUnlucky(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 1 when given '2016'", () => {
    // Arrange
    var input = 2016
    var expected = 1
    
    // Act
    var actual = howUnlucky(input)

    // Assert
    expect(actual).toBe(expected)
})

it("should throw an error when given 'a'", () => {
    // Arrange
    const input = "a"
    const expected = "Input is not a valid year."

    // Act & Assert
    expect(() => howUnlucky(input)).toThrow(expected);
})

it("Should return 1 when given '2016'", () => {
    // Arrange
    var input = "2016"
    var expected = 1
    
    // Act
    var actual = howUnlucky(input)

    // Assert
    expect(actual).toBe(expected)
})