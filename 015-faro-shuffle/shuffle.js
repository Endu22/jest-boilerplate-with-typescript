export function shuffleCount(deckSize) {
  if (deckSize < 2) throw new Error("Deck too small.")

  // Create deck
  let deck = getDeck(deckSize)
  // Exclude card at index 0.
  let newDeck = deck.slice(1, deck.length)
  // Keep a copy of newDeck
  const initialDeck = newDeck

  let counter = 0
  do {
    counter = counter + 1
    newDeck = faroShuffle(newDeck) // do faroShuffle
  } while (!isEqual(initialDeck, newDeck)) // check if equal

  return counter
}

function faroShuffle(deck) {
  // divide
  let firstHalve = deck.slice(0, deck.length / 2)
  let secondHalve = deck.slice(deck.length / 2, deck.length)

  let returnArr = []
  for (let i = 0; i < deck.length / 2; i++) {
    // Always push from the secondHalve first.
    if (secondHalve[i] !== undefined) returnArr.push(secondHalve[i])
    if (firstHalve[i] !== undefined) returnArr.push(firstHalve[i])
  }

  return returnArr
}

// Construct deck.
function getDeck(size) {
  let returnArr = []

  for (let i = 0; i < size; i++) {
    returnArr.push(i + 1)
  }
  return returnArr
}

// check array equality.
function isEqual(initialDeck, newDeck) {
  for (let i = 0; i < initialDeck.length; i++) {
    if (initialDeck[i] !== newDeck[i]) {
      return false
    }
  }
  return true
}
