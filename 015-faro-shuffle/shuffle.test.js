import { shuffleCount } from "./shuffle"

it("Should return 3 when given '8", () => {
    // Arrange
    var input = 8
    var expected = 3
    
    // Act
    var actual = shuffleCount(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 12 when given '14", () => {
    // Arrange
    var input = 14
    var expected = 12
    
    // Act
    var actual = shuffleCount(input)

    // Assert
    expect(actual).toBe(expected)
})


it("Should return 8 when given '52", () => {
    // Arrange
    var input = 52
    var expected = 8
    
    // Act
    var actual = shuffleCount(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should throw and error when given 1", () => {
    // Arrange
    var input = 1

    // Act & Assert
    expect(() => shuffleCount(input)).toThrow("Deck too small.")
})