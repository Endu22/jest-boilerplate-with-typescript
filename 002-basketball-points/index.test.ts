/**
points(1, 1) ➞ 5
points(7, 5) ➞ 29
points(-2, 1) ➞ an error!
points(38, 8) ➞ 100
points(0, 1) ➞ 3
points(0, 0) ➞ 0 

*/

import { points } from ".";

// Arg 1: "Test description".
// Arg 2: The function we'd like to test.
it("should add one two-pointer and one three-pointer to equal 5", () => {
    // at least 1 assertion
    const total = points(1, 1);
    expect(total).toBe(5);
})

it("should add 7 two-pointers and 5 three-pointers to equal 29", () => {
    expect(points(7,5)).toBe(29);
})

it("should throw and error for a negative two-pointer", () => {
    expect(() => points(-2, 1)).toThrow("Negative two-points.");
})

it("should throw and error for a negative three-pointer", () => {
    expect(() => points(1, -2)).toThrow("Negative three-points.");
})