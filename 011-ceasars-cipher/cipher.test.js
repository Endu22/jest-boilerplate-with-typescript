import { caesarCipher } from "./cipher"

it("Should return 'okffng-Qwvb' when given (middle-Outz, 2)", () => {
    // Arrange
    var input = "middle-Outz"
    var input2 = 2
    var expected = "okffng-Qwvb"
    
    // Act
    var actual = caesarCipher(input, input2)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 'Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj' when given Always-Look-on-the-Bright-Side-of-Life, 5)", () => {
    // Arrange
    var input = "Always-Look-on-the-Bright-Side-of-Life"
    var input2 = 5
    var expected = "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
    
    // Act
    var actual = caesarCipher(input, input2)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 'Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj' when given Always-Look-on-the-Bright-Side-of-Life, 5)", () => {
    // Arrange
    var input = "Always-Look-on-the-Bright-Side-of-Life"
    var input2 = 5
    var expected = "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
    
    // Act
    var actual = caesarCipher(input, input2)

    // Assert
    expect(actual).toBe(expected)
})