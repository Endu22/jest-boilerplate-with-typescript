export function caesarCipher(inputString, numToAddToCharCode) {
  let cipher = []
  let unCiphered = []

  for (let i = 0; i < inputString.length; i++) {
    if (inputString[i] === "z") {
      cipher.push(
        String.fromCharCode(
          inputString.charCodeAt(i) - (22 + numToAddToCharCode)
        )
      )
    } else if (inputString[i] === "w") {
        cipher.push(
            String.fromCharCode(
              inputString.charCodeAt(i) - (16 + numToAddToCharCode)
            )
          )
    } else if (inputString[i] === "y") {
        cipher.push(
            String.fromCharCode(
              inputString.charCodeAt(i) - (16 + numToAddToCharCode)
            )
          )
    }else if (inputString[i] === "-") {
      cipher.push(String.fromCharCode(inputString.charCodeAt(i)))
    } else {
      unCiphered.push(inputString.charCodeAt(i))
      cipher.push(
        String.fromCharCode(inputString.charCodeAt(i) + numToAddToCharCode)
      )
    }
  }

  return cipher.join("")
}
