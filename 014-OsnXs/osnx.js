/** --------------------------------------
 *             Did not pass.
    --------------------------------------*/ 

export function XandO(board) {
  let boardArr = [board[0].split("|"), board[1].split("|"), board[2].split("|")]

  //    console.log(findHorizontalWin(boardArr, 0, 0, 0));

  return findHorizontalWin(boardArr, 0, 0, 0)
}

function findHorizontalWin(boardArr, i, j, counter) {
  console.log(counter)

  if (i === 3 && j === 3) {
    return false
  }

  if (j === 3) {
    i++
    j = 0
  }

  if (i === 3 && j === 3) {
    return false
  }

  if (counter === 2) return [i + 1, j + 1]

  if (boardArr[i][j] === "X") {
    return findHorizontalWin(boardArr, i, j++, counter++)
  } else {
    return findHorizontalWin(boardArr, i++, j, counter)
  }
}
