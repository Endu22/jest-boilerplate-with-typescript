import { XandO } from "./osnx"

it("Should return '[1, 3]' when given '[' | | ', ' |X| ', 'X| | ']'", () => {
    // Arrange
    var input = [' | | ', ' |X| ', 'X|X| ']
    var expected = [3, 3]
    
    // Act
    var actual = XandO(input)

    // Assert
    expect(actual).toStrictEqual(expected)
})