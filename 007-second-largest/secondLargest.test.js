import { secondLargest } from "./secondLargest"

it("Should return 40 when given [10, 40, 30, 20, 50]", () => {
    // Arrange
    var input = [10, 40, 30, 20, 50]
    var expected = 40 
    
    // Act
    var actual = secondLargest(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 105 when given [25, 143, 89, 13, 105]", () => {
    // Arrange
    var input = [25, 143, 89, 13, 105]
    var expected = 105 
    
    // Act
    var actual = secondLargest(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 23 when given [54, 23, 11, 17, 10]", () => {
    // Arrange
    var input = [54, 23, 11, 17, 10]
    var expected = 23 
    
    // Act
    var actual = secondLargest(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 0 when given [1, 1]", () => {
    // Arrange
    var input = [1, 1]
    var expected = 0 
    
    // Act
    var actual = secondLargest(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 1 when given [1]", () => {
    // Arrange
    var input = [1]
    var expected = 1 
    
    // Act
    var actual = secondLargest(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 0 when given []", () => {
    // Arrange
    var input = []
    var expected = 0 
    
    // Act
    var actual = secondLargest(input)

    // Assert
    expect(actual).toBe(expected)
})