export function secondLargest(inputArr) {
  if (inputArr.length === 0) {
    return 0
  }

  if (inputArr.length === 1) {
    return inputArr[0]
  }

  if (inputArr.length === 2) {
    if (inputArr[0] === inputArr[1]) {
      return 0
    } else {
      return inputArr[0] > inputArr[1] ? inputArr[0] : inputArr[1]
    }
  }

  inputArr.sort((a, b) => {
    return b - a
  })

  return inputArr[1]
}
