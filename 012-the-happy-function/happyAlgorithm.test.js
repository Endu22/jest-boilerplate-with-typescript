import { happyAlgorithm } from "./happyAlgorithm"


it("Should return 'HAPPY 5' when given 139)", () => {
    // Arrange
    var input = 139
    var expected = "HAPPY 5"
    
    // Act
    var actual = happyAlgorithm(input)

    // Assert
    expect(actual).toBe(expected)
})


it("Should return 'SAD 10' when given 67)", () => {
    // Arrange
    var input = 67
    var expected = "SAD 10"
    
    // Act
    var actual = happyAlgorithm(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 'HAPPY 1' when given 1)", () => {
    // Arrange
    var input = 1
    var expected = "HAPPY 1"
    
    // Act
    var actual = happyAlgorithm(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 'SAD 8' when given 89)", () => {
    // Arrange
    var input = 89
    var expected = "SAD 8"
    
    // Act
    var actual = happyAlgorithm(input)

    // Assert
    expect(actual).toBe(expected)
})