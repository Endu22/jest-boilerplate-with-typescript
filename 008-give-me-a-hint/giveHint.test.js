import { giveTheHint } from "./giveHint"

it("Should return an array which contains strings which gradually reveals 'Mary Queen of Scots'", () => {
    // Arrange
    var input = "Mary Queen of Scots"
    var expected = ['____ _____ __ _____', 'M___ Q____ o_ S____', 'Ma__ Qu___ of Sc___', 'Mar_ Que__ of Sco__', 'Mary Quee_ of Scot_', 'Mary Queen of Scots'] 
    
    // Act
    var actual = giveTheHint(input)

    // Assert
    expect(actual).toStrictEqual(expected)
})


it("Should return an array which contains strings which gradually reveals 'The Life of Pi'", () => {
    // Arrange
    var input = 'The Life of Pi'
    var expected = ['___ ____ __ __', 'T__ L___ o_ P_', 'Th_ Li__ of Pi', 'The Lif_ of Pi', 'The Life of Pi'] 
    
    // Act
    var actual = giveTheHint(input)

    // Assert
    expect(actual).toStrictEqual(expected)
})