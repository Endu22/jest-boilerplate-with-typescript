export function giveTheHint(inputString) {
  let returnArr = []

  // get longest word in string
  let words = inputString.split(" ")
  words.sort((a, b) => {
    return b.length - a.length
  })

  console.log(words[0])

  const longestWordLength = words[0].length

  for (let i = -1; i < longestWordLength; i++) {
    let aString = inputString
      .split(" ")
      .map((word) => {
        let newWord = ""
        for (let index = 0; index < word.length; index++) {
          if (i === -1) {
            newWord += "_"
          } else if (index <= i) {
            newWord += word.at(index)
          } else {
            newWord += "_"
          }
        }
        return newWord
      })
      .join(" ")

    returnArr.push(aString)
  }

  return returnArr
}
