export function testJackpot(inputArr) {
  if (inputArr.length < 4) throw new Error("Less than 4 items in array")

  const firstString = inputArr[0]
  for (let i = 1; i < inputArr.length; i++) {
    if (firstString !== inputArr[i]) {
      return false
    }
  }
  return true
}
