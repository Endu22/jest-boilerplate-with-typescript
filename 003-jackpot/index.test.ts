/**
testJackpot(["@", "@", "@", "@"]) ➞ true
testJackpot(["abc", "abc", "abc", "abc"]) ➞ true
testJackpot(["SS", "SS", "SS", "SS"]) ➞ true
testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false
testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false
 */

import { testJackpot } from ".";

it("should return true when array only contain @'s", () => {
    expect(testJackpot(["@", "@", "@", "@"])).toBe(true);
})

it("should return true when array only contain abc's", () => {
    expect(testJackpot(["abc", "abc", "abc", "abc"])).toBe(true);
})

it("should return true when array only contain SS's", () => {
    expect(testJackpot(["SS", "SS", "SS", "SS"])).toBe(true);
})

it("should return false when array equal ['&&', '&', '&&&', '&&&&']", () => {
    expect(testJackpot(['&&', '&', '&&&', '&&&&'])).toBe(false);
})

it("should return false when array equal ['SS', 'SS', 'SS', 'Ss']", () => {
    expect(testJackpot(['SS', 'SS', 'SS', 'Ss'])).toBe(false);
})

it("should throw and error when array size is less than 4", () => {
    expect(() => testJackpot(["abc", "abc"])).toThrow("Less than 4 items in array")
})

