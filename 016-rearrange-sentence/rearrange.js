export function rearrange(inputString) {
  let stringsArr = inputString.split(" ")
  let sortedArr = []

  for (let i = 1; i < stringsArr.length + 1; i++) {
    for (let j = 0; j < stringsArr.length; j++) {
      if (stringsArr[j].includes(i)) {
        sortedArr.push(stringsArr[j])
      }
    }
  }

  sortedArr = sortedArr.map((word) => {
    let newWord = []
    for (let i = 0; i < word.length; i++) {
      if (!word[i].match(/[1-9]/)) newWord.push(word[i])
    }
    return newWord.join("")
  })

  return sortedArr.join(" ")
}
