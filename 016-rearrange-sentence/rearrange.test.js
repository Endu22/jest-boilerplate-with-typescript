import { rearrange } from "./rearrange"

it("Should return 'This is a Test' when given 'is2 Thi1s T4est 3a'", () => {
    // Arrange
    var input = "is2 Thi1s T4est 3a"
    var expected = "This is a Test"
    
    // Act
    var actual = rearrange(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 'For the good of the people' when given '4of Fo1r pe6ople g3ood th5e the2'", () => {
    // Arrange
    var input = "4of Fo1r pe6ople g3ood th5e the2"
    var expected = 'For the good of the people'
    
    // Act
    var actual = rearrange(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return 'JavaScript is so damn weird' when given '5weird i2s JavaScri1pt dam4n so3'", () => {
    // Arrange
    var input = "5weird i2s JavaScri1pt dam4n so3"
    var expected = 'JavaScript is so damn weird'
    
    // Act
    var actual = rearrange(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return '' when given ' '", () => {
    // Arrange
    var input = " "
    var expected = ''
    
    // Act
    var actual = rearrange(input)

    // Assert
    expect(actual).toBe(expected)
})
     
it("Should return 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed' when given 'elit8, ip2sum a5met, sed9 si4t con6sectetur adipisci7ng 3dolor Lore1m'", () => {
    // Arrange
    var input = 'elit8, ip2sum a5met, sed9 si4t con6sectetur adipisci7ng 3dolor Lore1m'
    var expected = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed'
    
    // Act
    var actual = rearrange(input)

    // Assert
    expect(actual).toBe(expected)
})