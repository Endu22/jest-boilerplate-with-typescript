import { elasticize } from "."


it("Should return ANNNNA when given ANNA", () => {
    // Arrange
    var input = "ANNA"
    var expected = "ANNNNA" 
    
    // Act
    var actual = elasticize(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return KAAYYYAAK when given KAYAK", () => {
    // Arrange
    var input = "KAYAK"
    var expected = "KAAYYYAAK" 
    
    // Act
    var actual = elasticize(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return X when given X", () => {
    // Arrange
    var input = "X"
    var expected = "X" 
    
    // Act
    var actual = elasticize(input)

    // Assert
    expect(actual).toBe(expected)
})

it("Should return YY when given YY", () => {
    // Arrange
    var input = "YY"
    var expected = "YY" 
    
    // Act
    var actual = elasticize(input)

    // Assert
    expect(actual).toBe(expected)
})