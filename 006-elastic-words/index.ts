export function elasticize(input) {
  var length = input.length

  if(length < 3)
    return input

  // Even
  if (length % 2 === 0) {
    var left = input.substring(0, length / 2)
    var right = input.substring(length / 2, length)

    // Create new left
    var newLeft = ""
    for (let i = 0; i < left.length; i++) {
        newLeft += left.at(i).repeat(i + 1)
    }

    // Create new right
    var newRight = newLeft.split("").reverse().join("")

    return newLeft + newRight
  }

  // Odd
  if (length % 2 !== 0) {
    var left = input.substring(0, length / 2)
    var right = input.substring(length / 2 + 1, length)
    var center = input.at(length / 2)

    // Create new left
    var newLeft = ""
    for (let i = 0; i < left.length; i++) {
        newLeft += left.at(i).repeat(i + 1)
    }

    // Create new pivot
    var newPivot = ""
    newPivot += center.repeat((length / 2) + 1)
    console.log(newPivot)

    // Create new right
    var newRight = newLeft.split("").reverse().join("")

    return newLeft + newPivot + newRight
  }
}
