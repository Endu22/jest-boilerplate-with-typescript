export function anagrams(inputString, inputArr) {
  let doMatchArr = []
  let doNotMatchArr = []
  let returnArr = []

  for (let i = 0; i < inputString.length; i++) {
    if (!doMatchArr.includes(inputString.at(i))) {
      doMatchArr.push(inputString[i])
    } else{
        doNotMatchArr.push(inputString[i])
    }
  }

  let regexStr = doMatchArr.join("")


  console.log(regexStr);
  
  let regex = /^[!?d]/ 
    //new RegExp(regexStr)

  inputArr.forEach(string => {
    if (string.match(regex)) {
        returnArr.push(string)
    }
  });

  console.log(returnArr);
  return returnArr
}
