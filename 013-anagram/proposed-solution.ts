// Here is an example of a solution
export const isAnagram = (wordA: string, anagrams: string[]) => {
    let result: string[] = [];

    //loop through the array
    for (const wordB of anagrams) {
        //sort the words
        const sortA: string = wordA.split('').sort().join();
        const sortB: string = wordB.split('').sort().join();

        //if the sorted words are equal, then they consists
        //of the same letters i.e. an anagram
        if( sortA === sortB ){
            result.push(wordB);
        }
     }
     
     return result;
}