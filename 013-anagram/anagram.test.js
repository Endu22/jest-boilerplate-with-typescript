import { anagrams } from "./anagram"

it("Should return '['aabb', 'bbaa']' when given '('abba', ['aabb', 'abcd', 'bbaa', 'dada'])'", () => {
    // Arrange
    var input1 = 'abba'
    var input2 = ['aabb', 'abcd', 'bbaa', 'dada']
    var expected = ['aabb', 'bbaa']
    
    // Act
    var actual = anagrams(input1, input2)

    // Assert
    expect(actual).toBe(expected)
})