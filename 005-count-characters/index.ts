export function charCount(charToLookFor, stringToCheck) {
  if (charToLookFor.length > 1)
    throw new Error("First argument contains too many characters.")

  var count = 0
  for (let index = 0; index < stringToCheck.length; index++) {
    if(stringToCheck.at(index) === charToLookFor)
        count++
  }
  return count
}
