// charCount("a", "edabit") ➞ 1

import { charCount } from "."

// charCount("c", "Chamber of secrets") ➞ 1

// charCount("B", "boxes are fun") ➞ 0

// charCount("b", "big fat bubble") ➞ 4

// charCount("e", "javascript is good") ➞ 0

// charCount("!", "!easy!") ➞ 2

// charCount("wow", "the universe is wow") ➞ error

it("Should return 1 when looking for 'a' in 'edabit'", () => {
    var firstArgument = "a"
    var secondArgument = "edabit"

    var expected = 1 
    
    expect(charCount(firstArgument, secondArgument)).toBe(expected)
})

it("Should return 1 when looking for 'c' in 'Chamber of secrets'", () => {
    var firstArgument = "c"
    var secondArgument = "Chamber of secrets"

    var expected = 1 
    
    expect(charCount(firstArgument, secondArgument)).toBe(expected)
})

it("Should return 0 when looking for 'B' in 'boxes are fun'", () => {
    var firstArgument = "B"
    var secondArgument = "boxes are fun"

    var expected = 0 
    
    expect(charCount(firstArgument, secondArgument)).toBe(expected)
})

it("Should return 4 when looking for 'b' in 'big fat bubble'", () => {
    var firstArgument = "b"
    var secondArgument = "big fat bubble"

    var expected = 4 
    
    expect(charCount(firstArgument, secondArgument)).toBe(expected)
})

it("Should return 0 when looking for 'e' in 'javascript is good'", () => {
    var firstArgument = "e"
    var secondArgument = "javascript is good"

    var expected = 0 
    
    expect(charCount(firstArgument, secondArgument)).toBe(expected)
})

it("Should return 2 when looking for '!' in '!easy!'", () => {
    var firstArgument = "!"
    var secondArgument = "!easy!"

    var expected = 2 
    
    expect(charCount(firstArgument, secondArgument)).toBe(expected)
})

it("Should throw an error when looking for 'wow' in 'the universe is wow'", () => {
  //
  var firstArgument = "wow"
  var secondArgument = "the universe is wow"

  var expected = "First argument contains too many characters."

  var actual = charCount(firstArgument, secondArgument)

  expect(actual).toBe(expected)
})
