export function isValidIP(inputIp) {
  const ipArr = inputIp.split(".")

  if (ipArr.length !== 4) {
    return false
  }

  for (let i = 0; i < ipArr.length; i++) {
    let ipPiece = ipArr[i]

    if (parseInt(ipPiece.at(0)) === 0) return false

    if (i === 3) {
      if (parseInt(ipPiece.at(ipPiece.length - 1)) === 0) return false
    }

    if (parseInt(ipPiece) < 1 || parseInt(ipPiece) > 255) return false
  }

  return true
}
