import { isValidIP } from "./validation"

// isValidIP("1.2.3.4") ➞ true
// isValidIP("1.2.3") ➞ false
// isValidIP("1.2.3.4.5") ➞ false
// isValidIP("123.45.67.89") ➞ true
// isValidIP("123.456.78.90") ➞ false
// isValidIP("123.456.78.89") ➞ false
// isValidIP("123.045.067.089") ➞ false



it("Should return true when given '1.2.3.4'", () => {
    // Arrange
    var input = "1.2.3.4"
    
    // Act
    var actual = isValidIP(input)

    // Assert
    expect(actual).toBeTruthy()
})


it("Should return false when given '1.2.3'", () => {
    // Arrange
    var input = '1.2.3'
    
    // Act
    var actual = isValidIP(input)

    // Assert
    expect(actual).toBeFalsy()
})

it("Should return false when given '1.2.3.4.5'", () => {
    // Arrange
    var input = "1.2.3.4.5"
    
    // Act
    var actual = isValidIP(input)

    // Assert
    expect(actual).toBeFalsy()
})

it("Should return true when given '123.45.67.89'", () => {
    // Arrange
    var input = "123.45.67.89"
    
    // Act
    var actual = isValidIP(input)

    // Assert
    expect(actual).toBeTruthy()
})



it("Should return false when given '123.456.78.90'", () => {
    // Arrange
    var input = "123.456.78.90"
    
    // Act
    var actual = isValidIP(input)

    // Assert
    expect(actual).toBeFalsy()
})

it("Should return false when given '123.045.067.089'", () => {
    // Arrange
    var input = "123.045.067.089"
    
    // Act
    var actual = isValidIP(input)

    // Assert
    expect(actual).toBeFalsy()
})

it("Should return false when given '123.456.78.89'", () => {
    // Arrange
    var input = "123.456.78.89"
    
    // Act
    var actual = isValidIP(input)

    // Assert
    expect(actual).toBeFalsy()
})