export function correctTitle(title) {
  var newString = title
  for (let index = 0; index < title.length; index++) {
    if (
      newString.charAt(index) === "," &&
      newString.charAt(index + 1) !== " "
    ) {
      newString = title.replace(",", ", ")
    }
  }

  newString = newString
    .split(" ")
    .map((word) => {
      var newWord = word.toLowerCase()
      if (newWord !== "and" && newWord !== "the" && newWord !== "of" && newWord !== "in") {
        newWord =
          newWord.charAt(0).toUpperCase() + newWord.substring(1, newWord.length)
      }
      return newWord
    })
    .join(" ")

    if(newString.charAt(newString.length-1) !== ".") {
        newString = newString + "."
    }

  return newString
}
