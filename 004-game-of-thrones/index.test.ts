// correctTitle("jOn SnoW, kINg IN thE noRth") ➞ "Jon Snow, King in the North." 

// correctTitle("sansa stark,lady of winterfell.") ➞ "Sansa Stark, Lady of 
// Winterfell." 
 
// correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.") ➞ "Tyrion Lannister, 
// Hand of the Queen."

import { correctTitle } from ".";

it("Should return: 'Jon Snow, King in the North'", () => {
    expect(correctTitle("jOn SnoW, kINg IN thE noRth")).toBe("Jon Snow, King in the North.");
})

it("Should return: 'Sansa Stark, Lady of Winterfell.'", () => {
    expect(correctTitle("sansa stark,lady of winterfell.")).toBe("Sansa Stark, Lady of Winterfell.");
})

it("Should return: 'Tyrion Lannister, Hand of the Queen.'", () => {
    expect(correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.")).toBe("Tyrion Lannister, Hand of the Queen.");
})